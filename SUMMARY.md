# Summary

* [Introduction](README.md)
* [株式会社Kronos 2014年度総括](kronos_2014/README.md)
* [大西AM特別インタビュー](am/README)
* [Javaエンジニア必見 Java8の新機能](java_java8/README)
   * [広島](java_java8)
   * [進藤](java_java8)
* [新人エンジニアが語る現場で本当に役立つスキル](README)
   * [澤村](.)
   * [鈴木](.)
   * [田代](.)
   * [千脇](.)
   * [當野](.)
   * [西島](.)
* [特別企画1 Kronos Rootsの舞台裏](1_kronos_roots/README)
* [特別企画2 ルー語が話せない人必見！ ルー語を始める上で確実に押さえておきたい20語](20/README.md)

