# 株式会社Kronos 2014年度総括

# 5月
### 日経ソフトウェアにSphero記事掲載
![](http://ecx.images-amazon.com/images/I/61AKowm6b7L._SL500_AA300_.jpg)

### 新入社員研修、感動のフィナーレ
![](https://lh6.googleusercontent.com/-VC5QT_g-iY0/U6AVHrAzWdI/AAAAAAAADbU/1-fn3l_pEx8/w798-h531-no/%E5%85%A8%E5%93%A1%E9%9B%86%E5%90%88.jpg)

### 村山AMが散髪
![](https://lh4.googleusercontent.com/-26TtOIjY3iM/U6AU-oLlMYI/AAAAAAAADbU/GX3hh2GEuTw/w798-h531-no/DSC_1029.jpg)

# 6月
### Swiftの発表でiOS開発者びっくり
![](http://rack.1.mshcdn.com/media/ZgkyMDE0LzA3LzEyLzJhL3N3aWZ0bG9nb2hlLjU3NzBhLmpwZwpwCXRodW1iCTk1MHg1MzQjCmUJanBn/99456315/0f8/swift-logo-hero.jpg)

### Twelve-Factor Appスレッドが盛り上がる
![](http://cdn-ak.f.st-hatena.com/images/fotolife/m/mi_kattun/20130806/20130806205749.png)

### IT CARETプロジェクト始動
![](https://fbexternal-a.akamaihd.net/safe_image.php?d=AQC6VYpC5S2CWAgi&w=154&h=154&url=http%3A%2F%2Fitcaret.com%2Fimg%2Fitcaret_og.png)

# 7月
### 佐野さん、當野さんクールに入社
![](http://blogimg.goo.ne.jp/user_image/1c/ec/a759c7553755b5c715a4ef9a92a89838.jpg)<br>
**佐野実（さのみのる）実業家**
<br><br>
![](http://up.gc-img.net/post_img_web/2014/11/fac1fdbe0801d4a4a2ebe480e995651d_598.jpeg)<br>
**遠野なぎこ（とおのなぎこ）女優**

### Yammerにカメラスレッドが増加
![](http://trendy.nikkeibp.co.jp/lc/jidai/061227_prof.jpg)<br>
**篠山紀信（しのやまきしん）写真家**

### Java Silverみんな無事合格！
![](http://ecx.images-amazon.com/images/I/51zvd5HzX4L.jpg)

# 8月
### KronosRoots 第１号発刊
![](http://straightpress.jp/wp-content/uploads/2011/08/ae46b1f460ee46f789c27b264a6cb421183.jpg)

### 太田SLによるウェアラブル研究会発表
![](https://bytebucket.org/necoyama3/kronos-db-press/raw/46ca3fb3266a8a18eccc57912884cc2725295e5a/kronos_2014/img/20140806_006.jpg)

### 東京湾納涼船ツアー開催
![](https://bytebucket.org/necoyama3/kronos-db-press/raw/46ca3fb3266a8a18eccc57912884cc2725295e5a/kronos_2014/img/22_40_50.jpg)

# 9月
### ダブルタカシによるJava8勉強会開催
![](http://image.slidesharecdn.com/ftd-java8-140329124027-phpapp01/95/java8-1-638.jpg?cb=1396373752)

### 推しボンがプチブームに
![](https://bytebucket.org/necoyama3/kronos-db-press/raw/46ca3fb3266a8a18eccc57912884cc2725295e5a/kronos_2014/img/images.jpeg)

### エンジニア会後、沖縄VS宮崎飲み会
![](https://bytebucket.org/necoyama3/kronos-db-press/raw/46ca3fb3266a8a18eccc57912884cc2725295e5a/kronos_2014/img/20140919_003.jpg)
<br>
**大阪事務所　宮崎飲み会**

# 10月
### 谷本Mによる大阪UX勉強会開催
![](https://www.yammer.com/api/v1/uploaded_files/24764427/preview/DSC_4638.jpg)

### 突然のUnity勉強会開催
![](http://hnut.co.jp/blog/wp-content/uploads/2013/11/unity.jpg)

### bashの脆弱性問題でちょっと焦る
![](http://livedoor.blogimg.jp/applechinfo/imgs/b/d/bd985f1e.jpg)

# 11月
### 登山部始動
![](https://bytebucket.org/necoyama3/kronos-db-press/raw/46ca3fb3266a8a18eccc57912884cc2725295e5a/kronos_2014/img/500_10503785.jpg)

### 健康診断
![](http://d13n9ry8xcpemi.cloudfront.net/photo/odai/400/141bc3641ee1d66ffcc1a159a436041b_400.jpg)

### 年末調整
![](http://serif.hatelabo.jp/images/cache/bc8f7f7cc511e49477ac52273262f506da1a03fa/9906edba04a89219f5e2c89471344c58d2a4b1e0.gif)